const std = @import("std");
const toml = @import("toml");
var allocator = std.heap.page_allocator;

pub fn main()!void {
    const input = try std.io.getStdIn().readToEndAlloc(allocator, 2048);
    var output = std.ArrayList(u8).init(allocator);
    var vt = try toml.parse(allocator, input, null);
    defer {
        allocator.free(input);
        output.deinit();
        vt.deinit();
    }
    
    try toml.table2json(vt.root, output.writer(), true);
    try std.io.getStdOut().writer().writeAll(output.items);
}

